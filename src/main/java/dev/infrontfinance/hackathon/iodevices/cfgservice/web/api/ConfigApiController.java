package dev.infrontfinance.hackathon.iodevices.cfgservice.web.api;

import dev.infrontfinance.hackathon.iodevices.cfgservice.api.ConfigApi;
import dev.infrontfinance.hackathon.iodevices.cfgservice.model.Config;
import dev.infrontfinance.hackathon.iodevices.cfgservice.model.PostConfigResponse;
import dev.infrontfinance.hackathon.iodevices.cfgservice.service.ConfigService;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ConfigApiController implements ConfigApi {

  private final ConfigService configService;

  @Override
  public ResponseEntity<PostConfigResponse> deleteConfigWithID(final UUID configID) {
    return new ResponseEntity<>(configService.deleteConfig(configID), HttpStatus.OK);
  }

  @Override
  public ResponseEntity<List<Config>> getAllConfigs() {
    return new ResponseEntity<>(configService.getAllConfigs(), HttpStatus.OK);
  }

  @Override
  public ResponseEntity<Config> getConfigWithID(final UUID configID) {
    return new ResponseEntity<>(configService.getConfigWithID(configID), HttpStatus.OK);
  }

  @Override
  public ResponseEntity<PostConfigResponse> postConfig(@Valid final List<Config> config) {
    return new ResponseEntity<>(configService.saveConfigs(config), HttpStatus.OK);
  }

  @Override
  public ResponseEntity<PostConfigResponse> putUpdateWithID(final UUID configID, final Config config) {
    return new ResponseEntity<>(configService.replaceConfig(configID, config), HttpStatus.OK);
  }
}
