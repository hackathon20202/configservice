package dev.infrontfinance.hackathon.iodevices.cfgservice.service;

import static java.util.Objects.isNull;

import dev.infrontfinance.hackathon.iodevices.cfgservice.model.Action;
import dev.infrontfinance.hackathon.iodevices.cfgservice.model.Action.ActionTypeEnum;
import dev.infrontfinance.hackathon.iodevices.cfgservice.model.Config;
import dev.infrontfinance.hackathon.iodevices.cfgservice.model.PostConfigResponse;
import dev.infrontfinance.hackathon.iodevices.cfgservice.storage.StorageController;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConfigService {

  private final StorageController storageController;

  public List<Config> getAllConfigs() {
    final List<Config> resultList = storageController.getAllConfigs();
    if (resultList.isEmpty()) {
      log.info("Adding dummy value because we have nothing stored yet");
      resultList.add(new Config()
          .configId(UUID.randomUUID().toString())
          .name("DummyConfig")
          .addActionsItem(new Action()
              .actionID(UUID.randomUUID().toString())
              .name("DummyAction")
              .target("vwdpm.exe")
              .value("W")
              .actionType(ActionTypeEnum.SENDKEY)));
    }
    return resultList;

  }

  public PostConfigResponse saveConfigs(final List<Config> configs) {
    try {
      if (isNull(configs) || configs.isEmpty()) {
        return new PostConfigResponse().message("No Configs to save");
      }
      storageController.saveConfigs(configs);
      return new PostConfigResponse().message(String.format("All %d Configs saved", configs.size()));
    } catch (Exception e) {
      return new PostConfigResponse().message("Could not save Configs");
    }
  }

  public PostConfigResponse deleteConfig(final UUID configID) {
    try {
      storageController.deleteConfig(configID);
      return new PostConfigResponse().message("config deleted");
    } catch (Exception e) {
      return new PostConfigResponse().message(String.format("could not delete config %s", configID));
    }
  }

  public Config getConfigWithID(final UUID configID) {
    Config config = storageController.getConfigWithID(configID);
    if (isNull(config)) {
      log.error("Could not get Config. returning empty one");
      return new Config();
    }
    return config;
  }

  public PostConfigResponse replaceConfig(final UUID configID, final Config config) {
    try {
      storageController.replaceConfig(configID, config);
      return new PostConfigResponse().message(String.format("config with id %s updated", configID));
    } catch (Exception e) {
      return new PostConfigResponse().message(String.format("could not update config with id %s", configID));
    }

  }
}
