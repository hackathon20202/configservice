package dev.infrontfinance.hackathon.iodevices.cfgservice.storage;

import static java.util.Objects.isNull;

import dev.infrontfinance.hackathon.iodevices.cfgservice.model.Config;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StorageController {

  Map<String, Config> configHashMap = new HashMap<>();

  public void saveConfigs(@NonNull final List<Config> configs) {

    configs.forEach(this::saveConfig);
  }

  private void saveConfig(final Config config) {
    if (isNull(config)) {
      return;
    }
    if (configHashMap.containsKey(config.getConfigId())) {
      configHashMap.replace(config.getConfigId(), config);
    } else {
      configHashMap.putIfAbsent(config.getConfigId(), config);
    }
  }

  public List<Config> getAllConfigs() {
    return new ArrayList<>(configHashMap.values());
  }

  public void deleteConfig(final UUID configID) {
    configHashMap.remove(configID.toString());
  }

  public Config getConfigWithID(final UUID configID) {
    return configHashMap.getOrDefault(configID.toString(), null);
  }

  public void replaceConfig(final UUID configID, final Config config) {
    if (!configID.toString().equals(config.getConfigId())) {
      log.info("we do have a different id in the request as in the actual config");
      config.setConfigId(configID.toString());
    }
    saveConfig(config);
  }
}
